package ir.malv.buytheway.model.database.entities

/* Creator: Mahdi on 3/6/2018. */

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.ForeignKey.CASCADE
import android.arch.persistence.room.Index

@Entity(tableName = "deal",
        indices =
        [(Index(value = arrayOf("customer_phone", "property_id"), unique = true))],
        primaryKeys = ["property_id", "customer_phone"],
        foreignKeys = [(ForeignKey(entity = Property::class,
                parentColumns = arrayOf("id"),
                childColumns = arrayOf("property_id"))),
            (ForeignKey(entity = Customer::class,
                    parentColumns = arrayOf("phone"),
                    childColumns = arrayOf("customer_phone"),
                    onUpdate = CASCADE))
        ])
data class Deal(
        @ColumnInfo(name = "customer_phone") var customerPhone: String,
        @ColumnInfo(name = "property_id") var propertyId: Int,
        @ColumnInfo(name = "date_done") var dateDone: Long
)
