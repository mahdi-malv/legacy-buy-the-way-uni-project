package ir.malv.buytheway.model.database.entities

/* Creator: Mahdi on 3/6/2018. */

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * @see Entity
 * #check Room library by google
 * #Check data classes
 * @param phone is the Primary key
 * @param name is Customer name
 * @param pass is String password
 *
 * @author Mahdi Malvandi
 *
 */
@Entity(
        tableName = "customer"
)
data class Customer(
        @ColumnInfo(name = "name") var name: String,
        @ColumnInfo(name = "password") var pass: String,
        @PrimaryKey
        @ColumnInfo(name = "phone") var phone: String
)