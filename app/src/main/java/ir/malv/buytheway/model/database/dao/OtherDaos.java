package ir.malv.buytheway.model.database.dao;
/* Creator: Mahdi on 3/6/2018. */

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import io.reactivex.Single;

@Dao
public interface OtherDaos {

    /**
     * Admin can verify a property so it can be shown to users as a Valid property to deal.
     * If a property is not verified it will be saved with adminCode = -1
     * @param adminCode code of admin doing verification
     * @param id is id of selected property
     */
    @Query("UPDATE property SET admin_code = :adminCode where id = :id")
    void verifyProperty(int adminCode, int id);

    /**
     * to Specify verification we need to know it's adminCode
     * If it's -1 then it's not verified
     * @param id is property id
     * @return adminCode of the property with id entered as param
     */
    @Query("SELECT admin_code FROM property WHERE ID = :id")
    Single<Integer> getAdminCode(int id);
}
