package ir.malv.buytheway.model.database.dao;
/* Creator: Mahdi on 3/6/2018. */

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import ir.malv.buytheway.model.database.entities.Customer;

@Dao
public interface CustomerDao {

    /**
     * @return list of all Customers
     * Flowable is a RxJava observable which allows to do iterating and getting return value Async
     * The reason is because Room doesn't allow doing queries on MainThread so we need to do it in another thread.
     */
    @Query("select * from customer")
    Flowable<List<Customer>> getAllCustomers();

    /**
     * @param phoneNum is the Phone
     * @return a Single Customer with phone
     * and Error if no Customer exists
     */
    @Query("select * from customer where phone = :phoneNum")
    Single<Customer> getByPhone(String phoneNum);

    /**
     * Insert all Customers
     * @param customers is An object of Class {@link Customer}
     * If there's a conflict (phone entered before) it's gonna fail
     */
    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insertAll(Customer... customers);

    /**
     * By giving the Object to this Function it will delete it from database.
     * @param customer is the Customer's Object (But we know phone matters).
     */
    @Delete
    void deleteCustomer(Customer customer);

    /**
     * This is same as previous one
     * @param phone is the Phone number of that customer
     */
    @Query("delete from customer where phone in (:phone)")
    void deleteByPhones(int[] phone);

    /**
     * Information of customer can be edited.
     * @param customer is Customer's Object
     * only Phone matters. It will update the info of the Customer with {@see customer.phoneNumber}
     */
    @Update
    void update(Customer customer);
}
