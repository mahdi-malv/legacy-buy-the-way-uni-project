package ir.malv.buytheway.model.database.dao;
/* Creator: Mahdi on 3/6/2018. */

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import org.intellij.lang.annotations.Flow;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import ir.malv.buytheway.model.database.entities.Property;

@Dao
public interface PropertyDao {

    /**
     *
     * @return List of all properties in a Flowable object.
     */
    @Query("select * from property")
    Flowable<List<Property>> getAllProperties();

    /**
     * Function to Filter property to be shown to user.
     * @param phone phone shouldn't be User's own. User doesn't buy his own property.
     * @param cityCode1 is the start Range of cities. It will be Index of cities in {@link ir.malv.buytheway.model.Const}
     * @param cityCode2 is end range
     * @param dTypes refer to {@link ir.malv.buytheway.model.Const . dealTypes}
     * @param pTypes refer to {@link ir.malv.buytheway.model.Const . propertyTypes}
     * @param fromSize is size start range
     * @param toSize is end of size range
     * @param fromPrice is Start of price range
     * @param toPrice is end of it.
     * @return a List of properties with info that matches users requirements
     */
    @Query("select * from property where (city_code between :cityCode1 and :cityCode2) and (dType in (:dTypes)) and (pType in (:pTypes)) " +
            "and (size between :fromSize and :toSize) and (price between :fromPrice and :toPrice) and" +
            " (owner_phone <> :phone) and (admin_code <> -1) and id not in (select property_id from deal)")
    Flowable<List<Property>> getFilteredProperties(String phone, int cityCode1, int cityCode2, String[] dTypes, String[] pTypes,
                                                  int fromSize, int toSize, int fromPrice, int toPrice);

    /**
     * A property that is NOT user's own, it's adminCode isn't -1 and is not in the deals is Available to deal.
     * @param phone is user's phone to filter and remove his properties from list.
     * @return a List of properties as Flowable.
     */
    @Query("select * from property where owner_phone <> :phone and admin_code <> -1 and id not in (select property_id from deal)")
    Flowable<List<Property>> getAvailableProperties(String phone);

    /**
     * @param id property Id
     * @return one Property in a Single object
     */
    @Query("select * from property where id = :id")
    Single<Property> getPropertyById(int id);

    @Insert
    void insertAll(Property... properties);

    /**
     * @return Return last property added.
     * Why? when user adds a property it will be checked by getting the Property again and checking info.
     */
    @Query("SELECT * FROM property ORDER BY id DESC LIMIT 1")
    Single<Property> getLastProperty();

    @Delete
    void deleteProperty(Property property);

    @Query("delete from property where owner_phone = :phone")
    void deleteByPhone(String phone);

    @Update
    void update(Property property);
}
