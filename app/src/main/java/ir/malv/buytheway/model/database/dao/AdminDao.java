package ir.malv.buytheway.model.database.dao;
/* Creator: Mahdi on 3/6/2018. */

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Single;
import ir.malv.buytheway.model.database.entities.Admin;

@Dao
public interface AdminDao {

    /**
     * @return all admins
     */
    @Query("select * from admin")
    List<Admin> getAllAdmins();

    /**
     * This function is related to login stuff
     * It returns a {@link io.reactivex.Single} object that is used to do queries Async
     * Return list of admins
     * @param id is admin's Id
     * @return password of selected admin
     */
    @Query("select pass from admin where id = :id")
    Single<String> getAdminPassword(int id);

    /**
     * This function returns id and password of admin
     * @param id is admin id
     * @return a Maybe. {@link io.reactivex.Maybe}
     * is like single but it might return no result. So for no result you won't get error.
     */
    @Query("select * from admin where id = :id")
    Maybe<Admin> getAdminById(int id);

    /**
     * Add admin to database
     * @insert is the Insert annotation from room.
     * @param admin is an Object that has admin's id and password
     * It will throw exception if conflicted.
     */
    @Insert(onConflict = OnConflictStrategy.FAIL)
    void addAdmin(Admin admin);

    @Update
    void updateAdmin(Admin admin);

    @Delete
    void removeAdmin(Admin admin);
}
