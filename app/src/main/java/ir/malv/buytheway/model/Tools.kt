package ir.malv.buytheway.model

import android.app.Activity
import android.content.Context
import android.content.Context.INPUT_METHOD_SERVICE
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import java.io.File
import java.io.IOException
import java.util.regex.Pattern


class Tools {

    fun isFirstRun(c: Context, prefName: String): Boolean {
        val isFirstRun = c.getSharedPreferences(prefName, Context.MODE_PRIVATE).getBoolean("isFirstRun", true)
        if (isFirstRun) {
            c.getSharedPreferences(prefName, Context.MODE_PRIVATE).edit().putBoolean("isFirstRun", false).apply()
            return true
        }
        return false
    }

    fun getRealPathFromURI(c: Context, contentURI: Uri): String {
        val result: String
        val cursor = c.contentResolver.query(contentURI, null, null, null, null)
        if (cursor == null) { // Source is DropBox or other similar local file path
            result = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(/*MediaStore.Images.ImageColumns.DATA*/MediaStore.Files.FileColumns.DATA)
            result = cursor.getString(idx)
            cursor.close()
        }
        return result
    }

    fun isDeviceOnline(c: Context): Boolean {
        val cm = c.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo: NetworkInfo?
        netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

    companion object {
        fun validateEmail(s: String?) =
                s != null && !s.isEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(s).matches()

        fun validateUserName(s: String?): Boolean {
            val userNamePattern = "^[a-zA-Z0-9_]{3,15}$"
            val persianPattern = "^[ا-ی0-9_]{3,15}$"
            val pattern = Pattern.compile(userNamePattern)
            val persian = Pattern.compile(persianPattern)
            return s != null && !s.isEmpty() && (pattern.matcher(s).matches() || persian.matcher(s).matches())
        }

        fun validatePassword(s: String?): Boolean {
            val passwordPattern = Pattern.compile("[a-zA-Z0-9!@\\-_.#$%&]{4,24}")
            return s != null && !s.isEmpty() && passwordPattern.matcher(s).matches()
        }

        fun validateName(s: String): Boolean {
            val pattern = Pattern.compile("[a-zA-Z\\ ]{7,50}")
            val persianNamePattern = Pattern.compile("[ا-ی\\ ]{7,50}")
            return !s.isEmpty() && !s.startsWith(" ") && !s.contains("  ") &&
                    !s.endsWith(" ") && (pattern.matcher(s).matches() || persianNamePattern.matcher(s).matches())
        }

        fun validateValue(editText: EditText): Boolean {
            return validateValue(Integer.parseInt(editText.text.toString()))
        }

        fun validateValue(value: Int): Boolean {
            return value >= 0 && value.toString().length < 13
        }

        fun validatePhoneNumber(phone: String): Boolean {
            return phone.length == 11 && phone.startsWith("09")
        }

        fun getNumber(e: EditText): Int {
            if (e.text != null && !e.text.toString().isEmpty()) {
                try {
                    return Integer.parseInt(e.text.toString())
                } catch (ex: NumberFormatException) {
                    return -1
                }

            }
            return -1
        }

        fun getString(e: EditText?): String {
            return if (e == null || e.text == null) "" else e.text.toString()
        }

        fun getString(s: String?): String {
            return s ?: ""
        }

        fun initEditText(t: EditText, s: String, b: Bundle) {
            if (b.getString(s) != null && !b.getString(s)!!.isEmpty()) t.setText(b.getString(s))
        }

        @Throws(IOException::class)
        fun createFile(path: String, name: String): Boolean {
            val file = File(path)
            if (!file.exists()) file.mkdirs()
            val file1 = File(file, name)
            return file1.createNewFile()
        }

        fun hideSoftKeyboard(c: Activity) {
            if (c.currentFocus != null) {
                val inputMethodManager = c.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(c.currentFocus.windowToken, 0)
            }
        }

        fun showSoftKeyboard(c: Activity, view: View) {
            val inputMethodManager = c.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            view.requestFocus()
            inputMethodManager.showSoftInput(view, 0)
        }

        fun convertTimestampToDate(timestamp: Long) = PersianDate().getShamsi(timestamp)!!

        fun toast(c: Context, t: String, l: Int = Toast.LENGTH_SHORT) {
            Toast.makeText(c, t, l).show()
        }

        fun alert(c: Context, title: String, message: String): AlertDialog.Builder {
            return AlertDialog.Builder(c)
                    .setTitle(title)
                    .setMessage(message)
        }
    }
}
