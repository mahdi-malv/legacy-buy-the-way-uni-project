package ir.malv.buytheway.model.database.dao;
/* Creator: Mahdi on 3/6/2018. */

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import ir.malv.buytheway.model.database.Deals;
import ir.malv.buytheway.model.database.entities.Deal;

@Dao
public interface DealDao {

    /**
     * Flowable of list of all deals done by customer.
     * When a user selects a property to deal, it will be deleted from available properties for user. It will be appeared
     * in deal list for admin. If admin hits Ok it will be deleted from deals and sent to consultant for Rest of the operation.
     * @return list Of all deals in a Flowable object
     */
    @Query("select * from deal")
    Flowable<List<Deal>> getAll();

    /**
     * This format is for Showing to admins. Please refer to {@link Deals}
     * @return a Deals format of deal and customer and property.
     */
    @Query("select name as customer_name, dType, owner_name, date_done, owner_phone, customer_phone, property_id from (select date_done , customer_phone , dType , name as owner_name , owner_phone , customer_phone, property_id from (select date_done , customer_phone , dType , owner_phone , property_id from deal join property where property_id = id) join customer where owner_phone = phone) join customer where customer_phone = phone")
    Flowable<List<Deals>> getDeals();

    /**
     * @return a Single object of Deal with property id of it's property.
     * @param id is Property id
     */
    @Query("select * from deal where property_id = :id")
    Single<Deal> getDealByPropertyId(int id);

    /**
     * Insert a deal and Throws exception if there is a Conflict in the database.
     * @param deal is Deal object that holds all info.
     */
    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insert(Deal deal);

    @Update
    void update(Deal deal);

    @Delete
    void delete(Deal deal);
}
