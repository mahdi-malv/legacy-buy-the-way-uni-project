package ir.malv.buytheway.model.database

import android.arch.persistence.room.ColumnInfo

/**
 * Used in:
 * @see ir.malv.buytheway.model.database.dao.OtherDaos#getDeals
 */
data class Deals(@ColumnInfo(name = "customer_name") val customerName: String,
                 val dType: String,
                 @ColumnInfo(name = "owner_name") val ownerName: String,
                 @ColumnInfo(name = "date_done") val date: Long,
                 @ColumnInfo(name = "owner_phone") val ownerPhone: String,
                 @ColumnInfo(name = "customer_phone") val customerPhone: String,
                 @ColumnInfo(name = "property_id") val propertyId: Int)

data class Datum(val t1: String, val t2: String, val t3: String, val t4: String?)