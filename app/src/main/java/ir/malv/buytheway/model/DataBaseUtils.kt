package ir.malv.buytheway.model

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ir.malv.buytheway.model.database.AppDatabase
import ir.malv.buytheway.model.database.Datum
import ir.malv.buytheway.model.database.entities.Admin
import ir.malv.buytheway.model.database.entities.Customer
import ir.malv.buytheway.model.database.entities.Deal
import ir.malv.buytheway.model.database.entities.Property
import ir.malv.buytheway.model.user.UserStore
import ir.malv.buytheway.viewcontroller.activities.LoginActivity

//TODO: Document
/* Creator: Mahdi on 3/5/2018. */
class DataBaseUtils {

    //region Login-Register
    fun login(c: Context, phoneOrId: String, pass: String) {
        val activityResponse = c as Response
        val isManager = LoginActivity.isManager
        System.err.println("Login.isManager: $isManager")
        if (isManager) {
            //region Subscribe AdminLogging
            AppDatabase.getInstance(c)
                    .adminDao().getAdminById(phoneOrId.toInt())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.computation())
                    .subscribe({
                        System.err.println("Here we are with it = ${it.pass}")
                        if (it.pass == pass) {
                            UserStore(c).saveUser(it)
                        }
                        activityResponse.onComplete(it.pass == pass, Const.MANAGER_LOGIN)
                    }, {
                        activityResponse.onComplete(false, Const.MANAGER_LOGIN)
                    })
            //endregion
        } else {
            //region CustomerLogging
            AppDatabase.getInstance(c)
                    .customerDao()
                    .getByPhone(phoneOrId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.computation())
                    .subscribe({
                        if (it.pass == pass)
                            UserStore(c).saveUser(it)
                        activityResponse.onComplete(it.pass == pass, Const.CUSTOMER_LOGIN)
                    }, {
                        activityResponse.onComplete(false, Const.CUSTOMER_LOGIN_FAILED)
                    })
            //endregion
        }
    }

    @Throws(SQLiteConstraintException::class)
    fun registerCustomer(c: Context, name: String, pass: String, phone: String) {
        val activityResponse = c as Response
        //region Insert to db with Runnable
        val registerAndCheckRunnable = Runnable {
            try {
                AppDatabase.getInstance(c)
                        .customerDao()
                        .insertAll(Customer(name = name, pass = pass, phone = phone))
            } catch (e: SQLiteConstraintException) {
                return@Runnable
            }
            AppDatabase.getInstance(c)
                    .customerDao()
                    .getByPhone(phone)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.computation())
                    .subscribe({
                        if (it.phone == phone)
                            UserStore(c).saveUser(it)
                        activityResponse.onComplete(it.phone == phone, Const.CUSTOMER_SIGNUP)
                    }, {
                        activityResponse.onComplete(false, Const.CUSTOMER_SIGNUP_FAILED)
                    })
        }
        //endregion
        Thread(registerAndCheckRunnable).start()
    }

    @Throws(SQLiteConstraintException::class)
    fun registerAdmin(c: Context, id: Int, pass: String) {
        val activityResponse = c as Response
        //region Insert admin with Runnable
        val registerAndCheckRunnable = Runnable {
            try {
                AppDatabase.getInstance(c)
                        .adminDao()
                        .addAdmin(Admin(id, pass))
            } catch (e: SQLiteConstraintException) {
                return@Runnable
            }
            AppDatabase.getInstance(c)
                    .adminDao()
                    .getAdminById(id)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.computation())
                    .subscribe({
                        if (it.id == id && it.pass == pass) {
                            //Save admin
                            UserStore(c).saveUser(it)
                        }
                        activityResponse.onComplete(it.id == id && it.pass == pass, Const.MANAGER_SIGNUP)
                    }, {
                        activityResponse.onComplete(false, Const.MANAGER_SIGNUP_FAILED)
                    })
        }
        //endregion
        Thread(registerAndCheckRunnable).start()

    }
    //endregion

    //region Add-delete-Update Property
    fun addProperty(c: Context, p: Property) {
        val activityResponse = c as Response
        val addPropertyRunnable = Runnable {
            //First Insert then Check the last one inserted and compare to make sure and return id
            AppDatabase.getInstance(c)
                    .propertyDao()
                    .insertAll(p)

            AppDatabase.getInstance(c)
                    .propertyDao()
                    .lastProperty
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.computation())
                    .subscribe(
                            {
                                activityResponse.onComplete(true, it.id)
                            },
                            {
                                activityResponse.onComplete(false, -1)
                            }
                    )
        }
        Thread(addPropertyRunnable).start()
    }
    //endregion

    //region Admin's Tasks

    /**
     * This is for Customer too
     * @param propertyId is id of Property
     * @return Result object
     */
    fun getProperty(c: Context, propertyId: Int) {
        val result = c as Result
        AppDatabase.getInstance(c)
                .propertyDao()
                .getPropertyById(propertyId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe({
                    result.onComplete(it, null, Const.RESULT_HAS_DATA)
                }, {
                    result.onComplete(null, it, Const.RESULT_NO_DATA)
                })
    }

    /**
     * @param c is for AppDataBase
     * @param adminId is admin's Id who verifies this property
     * @param propertyId is Unique id of selected property
     * @return Response object
     * Notice that it returns Observable's result as <b>Response</b> OnComplete method
     */
    fun verifyProperty(c: Context, adminId: Int, propertyId: Int) {
        val response = c as Response
        val verificationRunnable = Runnable {
            AppDatabase.getInstance(c)
                    .otherDaos()
                    .verifyProperty(adminId, propertyId)
            AppDatabase.getInstance(c)
                    .otherDaos()
                    .getAdminCode(propertyId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.computation())
                    .subscribe({
                        response.onComplete(it == adminId,
                                if (it == adminId) Const.VERIFIED else Const.NOT_VERIFIED)
                    }, {
                        response.onComplete(false, Const.NOT_VERIFIED)
                    })
        }
        Thread(verificationRunnable).start()
    }

    /**
     * @return List<Datum>
     *     Datum holds for texts
     *     1. PType
     *     2. DType
     *     3.Verified or Not
     *     4. is Nothing for this one But
     *          We need Id for OnClick so Forth will have Id
     */
    fun getProperties(c: Context) {
        val result = c as Result
        val list = ArrayList<Datum>()
        val d = AppDatabase.getInstance(c)
                .propertyDao()
                .allProperties
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe({
                    for (item in it) {
                        list.add(Datum(item.pType, item.dType,
                                if (item.adminCode == -1) "تایید نشده" else item.adminCode.toString()
                                , item.id.toString()))
                    }
                    result.onComplete(list, null, Const.RESULT_HAS_DATA)
                }, {
                    result.onComplete(null, it, Const.RESULT_NO_DATA)
                })
    }

    /**
     * @return Datum (customerName, dealType, ownerName AND {date,ownerPhone,customerPhone,propertyId}
     * but why like this?
     * To avoid duplication i return a Four String object
     */
    fun getDeals(c: Context) {
        val result = c as Result
        val list = ArrayList<Datum>()
        AppDatabase.getInstance(c)
                .dealDao()
                .deals
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe({
                    for (item in it) {
                        val lastText = "${item.date},${item.ownerPhone},${item.customerPhone},${item.propertyId}"
                        list.add(Datum(item.customerName, item.dType, item.ownerName, lastText))
                    }
                    result.onComplete(list, null, Const.RESULT_HAS_DATA)
                }, {
                    result.onComplete(null, it, Const.RESULT_NO_DATA)
                })
    }

    fun deleteDealAndProperty(c: Context, d: Deal, ownerPhone: String) {
        val deletionRunnable = Runnable {
            AppDatabase.getInstance(c)
                    .dealDao()
                    .delete(d)
            AppDatabase.getInstance(c)
                    .propertyDao()
                    .deleteByPhone(ownerPhone)
        }

        Thread(deletionRunnable).start()
    }

    fun getCustomers(c: Context) {
        val result = c as Result
        val list = ArrayList<Datum>()
        AppDatabase.getInstance(c)
                .customerDao()
                .allCustomers
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe({
                    for (item in it) {
                        list.add(Datum(item.name, item.phone, "", null))
                    }
                    result.onComplete(list, null, Const.RESULT_HAS_DATA)
                }, {
                    result.onComplete(null, it, Const.RESULT_NO_DATA)
                })
    }
    //endregion

    //region Customer's Options

    fun getAvailableProperties(c: Context, phone: String) {
        val result = c as Result
        AppDatabase.getInstance(c)
                .propertyDao()
                .getAvailableProperties(phone)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe({
                    result.onComplete(it, null, Const.RESULT_HAS_DATA)
                }, {
                    result.onComplete(null, it, Const.RESULT_NO_DATA)
                })
    }

    /**
     * Used in:
     * @see ir.malv.buytheway.viewcontroller.activities.customer.CustomerActivity
     * @return Filtered List of Properties
     * last method was not supporting filter but this one does
     * @return Result#onComplete() function with Result as List<Property>
     */
    fun getFilteredAvailableProperties(c: Context, phone: String, cityCode: Int,
                                       dTypes: Array<String>, pTypes: Array<String>,
                                       fromSize: Int, toSize: Int,
                                       fromPrice: Int, toPrice: Int) {
        val result = c as Result
        val cc1 = if (cityCode == -1) 0 else cityCode
        val cc2 = if (cityCode == -1) Const.cities.size else cityCode
        AppDatabase.getInstance(c)
                .propertyDao()
                .getFilteredProperties(phone, cc1, cc2, dTypes, pTypes, fromSize, toSize, fromPrice, toPrice)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe({
                    result.onComplete(it, null, Const.RESULT_HAS_DATA)
                }, {
                    result.onComplete(null, it, Const.RESULT_NO_DATA)
                })
    }

    fun addDeal(c: Context, d: Deal) {
        val response = c as Response
        val dealAddingRunnable = Runnable {
            AppDatabase.getInstance(c)
                    .dealDao()
                    .insert(d)
            AppDatabase.getInstance(c)
                    .dealDao()
                    .getDealByPropertyId(d.propertyId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.computation())
                    .subscribe({
                        response.onComplete(it.customerPhone == d.customerPhone, 0)
                    }, {
                        Log.e("DataBaseUtils#addDeal", it.message)
                        response.onComplete(false, 1)
                    })
        }
        try {
            Thread(dealAddingRunnable).start()
        } catch (e: SQLiteConstraintException) {
            Log.e("DataBaseUtils#addDeal", "Fell in catch ${e.message}")
            response.onComplete(false, 2)
        }
    }

    //endregion

    //region Non-database function
    /**
     * @param code is the city code
     * @return Name of city
     */
    fun getCityByCode(code: Int) = Const.cities[code]

    /**
     * @param city is name
     * @return code of city or -1 if not found
     */
    fun getCodeByCity(city: String) = Const.cities.indexOf(city)
    //endregion
}

interface Response {
    fun onComplete(result: Boolean, responseCode: Int)
}

interface Result {
    fun onComplete(result: Any?, e: Throwable?, responseCode: Int)
}