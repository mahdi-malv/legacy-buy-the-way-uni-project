package ir.malv.buytheway.model.database.entities

/* Creator: Mahdi on 3/6/2018. */

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "admin")
data class Admin(
        @PrimaryKey @ColumnInfo(name = "id") var id: Int,
        @ColumnInfo(name = "pass") var pass: String
)
