package ir.malv.buytheway.model.database

/* Creator: Mahdi on 3/6/2018. */

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import ir.malv.buytheway.model.database.dao.*
import ir.malv.buytheway.model.database.entities.*


/**
 * @getInstance is the main fun. You have to use it to access database
 */
@Database(entities = [(Customer::class), (Property::class), (Deal::class), (Admin::class)], version = 1, exportSchema = false)

abstract class AppDatabase : RoomDatabase() {

    abstract fun customerDao(): CustomerDao

    abstract fun propertyDao(): PropertyDao

    abstract fun adminDao(): AdminDao

    abstract fun dealDao(): DealDao

    abstract fun otherDaos(): OtherDaos

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
                }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(context.applicationContext,
                        AppDatabase::class.java, "Malv_Real_Estate.db")
                        // prepopulate the database after onCreate was called
                        .build()
    }
}

