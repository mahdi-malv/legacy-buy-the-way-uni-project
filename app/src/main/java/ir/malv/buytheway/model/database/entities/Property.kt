package ir.malv.buytheway.model.database.entities

/* Creator: Mahdi on 3/6/2018. */

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.ForeignKey.CASCADE
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "property",
        foreignKeys = (arrayOf(ForeignKey(entity = Customer::class,
                parentColumns = arrayOf("phone"),
                childColumns = arrayOf("owner_phone"),
                onUpdate = CASCADE, onDelete = CASCADE)))) //onDeleteCascade is gonna make a mess...
data class Property(
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Int = 0,
        @ColumnInfo(name = "owner_phone") var ownerPhone: String,
        @ColumnInfo(name = "size") var size: Int,
        @ColumnInfo(name = "age") var age: Int,
        @ColumnInfo(name = "dType") var dType: String,
        @ColumnInfo(name = "pType") var pType: String,
        @ColumnInfo(name = "city_code") var cityCode: Int,
        @ColumnInfo(name = "lat") var lat: Double,
        @ColumnInfo(name = "lng") var lng: Double,
        @ColumnInfo(name = "comment") var comment: String,
        @ColumnInfo(name = "admin_code") var adminCode: Int,
        @ColumnInfo(name = "date_created") var dateCreatedStamp: Long,
        @ColumnInfo(name = "price") var price: Int,
        @ColumnInfo(name = "ejare") var ejare: Int
)
