package ir.malv.buytheway.model.user

import android.content.Context
import android.content.SharedPreferences
import ir.malv.buytheway.model.Const
import ir.malv.buytheway.model.Const.ID
import ir.malv.buytheway.model.Const.LOG_CHECK
import ir.malv.buytheway.model.Const.NAME
import ir.malv.buytheway.model.Const.PASS
import ir.malv.buytheway.model.Const.PHONE
import ir.malv.buytheway.model.Const.USER_STORE
import ir.malv.buytheway.model.database.entities.Admin
import ir.malv.buytheway.model.database.entities.Customer


class UserStore(c: Context) {
    private val preferences: SharedPreferences = c.getSharedPreferences(USER_STORE, Context.MODE_PRIVATE)

    val userLogged: Boolean
        get() = preferences.getBoolean(LOG_CHECK, false)

    /**
     * @return -1 for Unspecified
     * @return 0 for Customer
     * @return 1 for admin
     * Notice that if
     */
    fun getUserType() = preferences.getInt(Const.USER_TYPE, -1)

    fun saveUser(user: Customer) {
        val editor = preferences.edit()
        editor.putInt(Const.USER_TYPE, Const.USER_TYPE_CUSTOMER)

        editor.putString(NAME, user.name)
        editor.putString(PASS, user.pass)
        editor.putString(PHONE, user.phone)

        setLogged(true)
        editor.apply()
    }

    fun saveUser(admin: Admin) {
        val editor = preferences.edit()
        editor.putInt(Const.USER_TYPE, Const.USER_TYPE_ADMIN)
        editor.putInt(ID, admin.id)
        editor.putString(PASS, admin.pass)

        setLogged(true)
        editor.apply()
    }

    fun getUser(): Any {
        return if (getUserType() == Const.USER_TYPE_CUSTOMER)
            getCustomer()
        else
            getAdmin()
    }

    fun getCustomer(): Customer {
        return Customer(
                preferences.getString(NAME, "NoName"),
                preferences.getString(PASS, "NoPass"),
                preferences.getString(PHONE, "NoPhone")
        )
    }

    fun getAdmin(): Admin {
        return Admin(
                preferences.getInt(ID, -1),
                preferences.getString(PASS, "NoPass")
        )
    }

    private fun setLogged(logged: Boolean) {
        val editor = preferences.edit()
        editor.putBoolean(LOG_CHECK, logged)
        editor.apply()
    }

    fun logout() {
        preferences.edit().clear().apply()
        setLogged(false)
    }
}
