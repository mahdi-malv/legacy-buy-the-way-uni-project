package ir.malv.buytheway.viewcontroller.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import ir.malv.buytheway.R
import ir.malv.buytheway.model.Const
import ir.malv.buytheway.model.user.UserStore
import ir.malv.buytheway.viewcontroller.activities.admin.AdminActivity
import ir.malv.buytheway.viewcontroller.activities.customer.CustomerActivity
import kotlinx.android.synthetic.main.activity_splash.*

/**
 * @see MyAppCompatActivity
 */
class SplashActivity : MyAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        loginLayout.visibility = View.GONE

        /**
         * onAnimationEnd() takes a function
         * In that it will check for Login
         * @see UserStore
         */
        animateIcon(image, onAnimationEnd = {
            val userStore = UserStore(this)
            //Check Login
            if (userStore.userLogged && userStore.getUserType() == Const.USER_TYPE_CUSTOMER)
                startActivity(Intent(this, CustomerActivity::class.java))
            else if (userStore.userLogged && userStore.getUserType() == Const.USER_TYPE_ADMIN)
                startActivity(Intent(this, AdminActivity::class.java))
            else {
                loginLayout.visibility = View.VISIBLE
                animateIcon(loginLayout, R.anim.slide_from_bottom, 1500, {})
            }
        })

        login.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
//            overridePendingTransition()
        }

        register.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
//            overridePendingTransition()
        }
    }

    private fun animateIcon(
            v: View,
            animResId: Int = R.anim.activity_splash_fade_icon,
            duration: Long = 2000, onAnimationEnd: () -> Unit
    ) {

        val animation = AnimationUtils.loadAnimation(this, animResId)
        animation.duration = duration
        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}

            override fun onAnimationEnd(animation: Animation) {
                onAnimationEnd()
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })
        v.startAnimation(animation)
    }

    private var backPressed = 0L
    override fun onBackPressed() {
        if (backPressed + 2000 > System.currentTimeMillis()) {
            super.onBackPressed()
            finishAffinity()
        } else {
            Toast.makeText(applicationContext, "برای خروج مجددا بازگشت را بزنید.", Toast.LENGTH_SHORT).show()
            backPressed = System.currentTimeMillis()
        }
    }
}
