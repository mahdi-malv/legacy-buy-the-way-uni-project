package ir.malv.buytheway.viewcontroller.activities.both

import android.os.Bundle
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import ir.malv.buytheway.R
import ir.malv.buytheway.model.Tools
import ir.malv.buytheway.viewcontroller.activities.MyAppCompatActivity


class ShowMapActivity : MyAppCompatActivity(), OnMapReadyCallback {

    private var mMap: GoogleMap? = null
    private lateinit var markerOptions: MarkerOptions

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_map)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val loc = LatLng(intent.getDoubleExtra("lat",
                (-1).toDouble()), intent.getDoubleExtra("lng", (-1).toDouble()))
        if (loc.latitude == (-1).toDouble() || loc.longitude == (-1).toDouble()) {
            Tools.toast(this, "موقعیت درست دریافت نشده یا درست ذخیره نشده است.")
            finish()
        }

        markerOptions = MarkerOptions().position(loc).title("مکان ملک")
        mMap!!.clear()
        mMap!!.addMarker(markerOptions)
        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(loc))
        mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 14.0f))
    }
}

