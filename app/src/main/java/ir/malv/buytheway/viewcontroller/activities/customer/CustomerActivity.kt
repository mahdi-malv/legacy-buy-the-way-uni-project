package ir.malv.buytheway.viewcontroller.activities.customer

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.*
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import ir.malv.buytheway.R
import ir.malv.buytheway.model.Const
import ir.malv.buytheway.model.DataBaseUtils
import ir.malv.buytheway.model.Result
import ir.malv.buytheway.model.Tools
import ir.malv.buytheway.model.database.entities.Customer
import ir.malv.buytheway.model.database.entities.Property
import ir.malv.buytheway.model.user.UserStore
import ir.malv.buytheway.viewcontroller.activities.LoginActivity
import ir.malv.buytheway.viewcontroller.activities.MyAppCompatActivity
import ir.malv.buytheway.viewcontroller.activities.SplashActivity
import ir.malv.buytheway.viewcontroller.activities.both.ShowMapActivity
import kotlinx.android.synthetic.main.activity_customer.*
import kotlinx.android.synthetic.main.activity_customer_filter.*
import kotlinx.android.synthetic.main.toolbar_1.view.*

class CustomerActivity : MyAppCompatActivity(), Result {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer)
        val t = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(t)
        t.toolText.text = "مشاهده ی املاک"
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        init()
        //region Clicks
        textFilter.setOnClickListener {
            if (sheetLayout.isExpended()) {
                sheetLayout.collapse()
                addFab.show()
            } else {
                sheetLayout.expand()
                addFab.hide()
            }
        }
        sheetLayout.setOnProgressListener {
            if (it == 0F)
                addFab.show()
            else
                addFab.hide()
        }
        addFab.setOnClickListener {
            startActivity(Intent(this, AddPActivity::class.java))
        }
        apply.setOnClickListener {
            getData(
                    city.selectedItemPosition -1,
                    dType.selectedItemPosition -1,
                    pType.selectedItemPosition -1,
                    size.selectedItemPosition,
                    price.selectedItemPosition
            )
        }
        //endregion
    }

    override fun onStart() {
        super.onStart()
        val store = UserStore(this)

        getData(-1, -1, -1, 0, 0)
    }

    private var backPressed: Long = 0
    override fun onBackPressed() {
        if (backPressed + 2000 > System.currentTimeMillis()) {
            super.onBackPressed()
            finishAffinity()
        } else {
            Toast.makeText(applicationContext, "برای خروج دوباره بازگشت را بزنید.", Toast.LENGTH_SHORT).show()
            backPressed = System.currentTimeMillis()
        }
    }

    /**
     * Will be invoked if any of SheetLayout values change
     * @param city: position of city spinner -1
     * @param dType: Just pass position of dType spinner -1
     * @param pType: Same as dType
     * @param size: pass position
     * @param price: same as size
     * All available for params (-1, -1, -1, 0, 0)
     *
     * @return Result by #onComplete()
     */
    private fun getData(city: Int, dType: Int, pType: Int, size: Int, price: Int) {
        val store = UserStore(this)
        //Make size from selected item
        val fs = if (size == 0 || size == 1) 0 else if (size == 2) 50 else 100
        val ts = if (size == 0 || size == 3) 100000 else if (size == 1) 50 else 100

        //Make price from selected item
        val fp = if (price == 0 || price == 1) 0 else if (price == 2) 50 else if (price == 3) 100 else 200
        val tp = if (price == 0 || price == 4) 100000 else if (price == 1) 50 else if (price == 2) 100 else 200

        //If user is Customer then Request from Database
        if (store.userLogged && store.getUserType() == Const.USER_TYPE_CUSTOMER) {
            DataBaseUtils().getFilteredAvailableProperties(this, (store.getUser() as Customer).phone,
                    city,
                    if (dType == -1) Const.dealTypes else arrayOf(Const.dealTypes[dType]),
                    if (pType == -1) Const.propertyTypes else arrayOf(Const.propertyTypes[pType]),
                    fs, ts,
                    fp, tp)
        } else {
            Tools.toast(this, "اطلاعات حساب شما صحیح نیستند. دوباره  وارد شوید.")
            finishAffinity()
            startActivity(Intent(this, SplashActivity::class.java))
        }
    }

    //region Menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menu?.add(0, 0, 0, "خروج از حساب")
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == 0) {
            val s = UserStore(this)
            Tools.alert(this, "خروج", "آیا قصد خروج دارید؟" + "\n" + "شماره تلفن: " + s.getCustomer().phone +
            "\n" + "نام: " + s.getCustomer().name)
                    .setPositiveButton("بله", {d, w ->
                        s.logout()
                        startActivity(Intent(this, LoginActivity::class.java))
                        finishAffinity()
                    })
                    .setNegativeButton("انصراف", null)
                    .create().show()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
    //endregion

    //region List
    private fun init() {
        val cities = arrayOf("همه").plus(Const.cities)
        val dTypes = arrayOf("همه", Const.dealTypes[0], Const.dealTypes[1])
        val pTypes = arrayOf("همه").plus(Const.propertyTypes)
        val sizeArray = arrayOf("همه", "کمتر از 50 متر", "بین 50 تا 100 متر", "بیشتر از 100 متر")
        val priceArray = arrayOf("همه", "کمتر از 50 میلیون تومان", "بین 50 تا 100 تومان", "بین 100 تا 200 میلیون تومان", "بیشتر از 200 میلیون تومان")

        city.adapter = ArrayAdapter(this, R.layout.spinner_item, cities)
        dType.adapter = ArrayAdapter(this, R.layout.spinner_item, dTypes)
        pType.adapter = ArrayAdapter(this, R.layout.spinner_item, pTypes)
        size.adapter = ArrayAdapter(this, R.layout.spinner_item, sizeArray)
        price.adapter = ArrayAdapter(this, R.layout.spinner_item, priceArray)
        list.layoutManager = LinearLayoutManager(this)
        list.setHasFixedSize(false)
    }

    inner class Adapter(val set: List<Property>) : RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(
                    LayoutInflater.from(parent.context)
                            .inflate(R.layout.property_item, parent, false)
            )
        }

        override fun getItemCount() = set.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = set[position]
            val city = DataBaseUtils().getCityByCode(item.cityCode)
            val title = item.pType + " در " + city
            holder.title.text = title
            holder.dType.text = item.dType
            holder.dType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_home, 0)
            val size = item.size.toString() + " متر"
            holder.size.text = size
            holder.size.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_size, 0)
            holder.date.text = Tools.convertTimestampToDate(item.dateCreatedStamp)
            holder.date.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_date, 0)
            val price = item.price.toString() + " میلیون تومان"
            holder.price.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_price, 0)
            holder.price.text = price
            holder.locationFab.setOnClickListener {
                startActivity(Intent(this@CustomerActivity, ShowMapActivity::class.java)
                        .putExtra("lat", item.lat)
                        .putExtra("lng", item.lng))
            }

            /**
             * More Info can be sent to NextPage
             */
            holder.itemView.setOnClickListener {
                startActivity(Intent(this@CustomerActivity, PropertyActivity::class.java)
                        .putExtra("id", item.id))
            }

        }

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val locationFab = itemView.findViewById<FloatingActionButton>(R.id.locationFab)!!
        val title = itemView.findViewById<TextView>(R.id.title)!!
        val dType = itemView.findViewById<TextView>(R.id.dType)!!
        val size = itemView.findViewById<TextView>(R.id.size)!!
        val date = itemView.findViewById<TextView>(R.id.date)!!
        val price = itemView.findViewById<TextView>(R.id.price)!!
    }

    //endregion

    @Suppress("UNCHECKED_CAST")
    override fun onComplete(result: Any?, e: Throwable?, responseCode: Int) {
        if (result != null && e == null && responseCode == Const.RESULT_HAS_DATA) {
            //We have data

            list.adapter = Adapter(result as List<Property>)
            list.adapter.notifyDataSetChanged()
        } else {
            Log.e("Customer#onComplete", e?.message)
            Tools.toast(this, "دریافت داده از دیتابیس ناموفق بود.")
        }
    }
}
