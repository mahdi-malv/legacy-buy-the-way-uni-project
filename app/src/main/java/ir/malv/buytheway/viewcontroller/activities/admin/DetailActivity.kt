package ir.malv.buytheway.viewcontroller.activities.admin

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ir.malv.buytheway.R
import ir.malv.buytheway.databinding.ActivityDetailBinding
import ir.malv.buytheway.databinding.Toolbar1Binding
import ir.malv.buytheway.model.*
import ir.malv.buytheway.model.database.Datum
import ir.malv.buytheway.model.database.entities.Deal
import ir.malv.buytheway.viewcontroller.activities.MyAppCompatActivity

/**
 * This class holds the details of all Admin's actions
 * It Just simply takes an intent attribute and specifies that
 * what action is requested
 * @see MyAppCompatActivity
 */
class DetailActivity : MyAppCompatActivity(), Result, Response {


    lateinit var binding: ActivityDetailBinding
    private var state = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        val v = binding.root
        setContentView(v)
        val t = Toolbar1Binding.bind(v)
        setSupportActionBar(t.toolbar)
        state = intent.getIntExtra(Const.ACTION_KEY, -1)
        t.toolText.text =
                when (state) {
                    Const.ACTION_SEE_DEALS -> "معاملات"
                    Const.ACTION_SEE_PROPERTIES -> "املاک ثبت شده"
                    Const.ACTION_SEE_CUSTOMERS -> "مشتریان"
                    else -> "Unspecified Title"
                }
    }

    override fun onStart() {
        super.onStart()
        init()
    }

    //region List
    private fun init() {
        binding.list.layoutManager = GridLayoutManager(this, 2)
        binding.list.setHasFixedSize(false)
        when (state) {
            Const.ACTION_SEE_CUSTOMERS -> {
                DataBaseUtils().getCustomers(this)
            }
            Const.ACTION_SEE_PROPERTIES -> {
                DataBaseUtils().getProperties(this)
            }
            Const.ACTION_SEE_DEALS -> {
                DataBaseUtils().getDeals(this)
            }
        }
    }

    inner class Adapter(val set: List<Datum>) : RecyclerView.Adapter<AnyViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnyViewHolder {
            return AnyViewHolder(
                    LayoutInflater.from(parent.context)
                            .inflate(R.layout.detail_item, parent, false)
            )
        }

        override fun getItemCount() = set.size

        @SuppressLint("SetTextI18n")
        override fun onBindViewHolder(holder: AnyViewHolder, position: Int) {

            var t1 = ""
            var t2 = ""
            var t3 = ""
            var t4 = ""
            when (state) {
                Const.ACTION_SEE_CUSTOMERS -> {
                    t1 = "نام: "
                    t2 = "تلفن: "
                    t3 = ""
                    t4 = ""
                    holder.tv3.visibility = View.GONE
                    holder.tv4.visibility = View.GONE
                }
                Const.ACTION_SEE_PROPERTIES -> {
                    t1 = "نوع ملک: "
                    t2 = "نوع معامله: "
                    t3 = "وضعیت تایید: "
                    holder.tv4.visibility = View.GONE
                }
                Const.ACTION_SEE_DEALS -> {
                    t1 = "نام مشتری: "
                    t2 = "نوع معامله: "
                    t3 = "صاحب: "
                    t4 = "تاریخ: "
                }
            }

            //For saving extra info returned for deals
            var ownerPhone = ""
            var customerPhone = ""
            var propertyId = 0
            var dateDone = 0L
            holder.tv1.text = t1 + set[position].t1
            holder.tv2.text = t2 + set[position].t2
            holder.tv3.text = t3 + set[position].t3
            if (set[position].t4 != null && state == Const.ACTION_SEE_DEALS) {
                val threePartText = set[position].t4!!
                val array = threePartText.split(",")
                dateDone = array[0].toLong()
                ownerPhone = array[1]
                customerPhone = array[2]
                propertyId = array[3].toInt()
                holder.tv4.text = t4 + Tools.convertTimestampToDate(dateDone)
            }

            holder.itemView.setOnClickListener {
                when (state) {
                    Const.ACTION_SEE_CUSTOMERS -> {
                        Tools.toast(this@DetailActivity, "No Action now. But you might be able to delete them.")
                    }
                    Const.ACTION_SEE_PROPERTIES -> {
                        startActivity(
                                Intent(this@DetailActivity, PropertyDetailActivity::class.java)
                                        .putExtra("id", set[position].t4?.toInt())
                        )
                    }
                    Const.ACTION_SEE_DEALS -> {
                        Tools.alert(this@DetailActivity,
                                "تکمیل معامله",
                                "آیا از ارسال اطلاعات معامله به مشاور املاک مطمئنید؟\n" +
                                        "تایید باعث حذف معامله و ملک از پایگاه داده میشود.")
                                .setPositiveButton("تایید") { d, _ ->
                                    Tools.toast(this@DetailActivity, "اوکی حذفش میکنم")
                                    val deal = Deal(customerPhone, propertyId, dateDone)
                                    DataBaseUtils().deleteDealAndProperty(this@DetailActivity,
                                            deal, ownerPhone)
                                    d.dismiss()
                                }
                                .setNegativeButton("انصراف", null)
                                .create()
                                .show()
                    }
                }
            }
        }

    }

    /**
     * Hold a 4 TextView data for all list actions
     */
    inner class AnyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tv1 = itemView.findViewById<TextView>(R.id.tv1)!!
        val tv2 = itemView.findViewById<TextView>(R.id.tv2)!!
        val tv3 = itemView.findViewById<TextView>(R.id.tv3)!!
        val tv4 = itemView.findViewById<TextView>(R.id.tv4)!!
    }
    //endregion

    /**
     * @param result is List<Datum> here. Always
     */
    @Suppress("UNCHECKED_CAST")
    override fun onComplete(result: Any?, e: Throwable?, responseCode: Int) {
        if (result != null && e == null) {
            binding.list.adapter = Adapter(result as List<Datum>)
            binding.list.adapter.notifyDataSetChanged()
        } else
            System.err.println("Error in DetailActivity#onComplete(): ${e?.message}")
    }

    override fun onComplete(result: Boolean, responseCode: Int) {

    }

}
