package ir.malv.buytheway.viewcontroller.activities.admin

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatDelegate
import android.util.Log
import android.view.View
import ir.malv.buytheway.R
import ir.malv.buytheway.databinding.ActivityPropertyDetailBinding
import ir.malv.buytheway.model.*
import ir.malv.buytheway.model.database.AppDatabase
import ir.malv.buytheway.model.database.entities.Admin
import ir.malv.buytheway.model.database.entities.Property
import ir.malv.buytheway.model.user.UserStore
import ir.malv.buytheway.viewcontroller.activities.MyAppCompatActivity
import ir.malv.buytheway.viewcontroller.activities.SplashActivity
import ir.malv.buytheway.viewcontroller.activities.both.ShowMapActivity
import kotlinx.android.synthetic.main.activity_property_detail.*
import kotlinx.android.synthetic.main.toolbar_1.*

class PropertyDetailActivity : MyAppCompatActivity(), Response, Result {

    private var adminCode = -2
    lateinit var binding: ActivityPropertyDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPropertyDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        // TODO: Complete the rest (link: https://developer.android.com/topic/libraries/view-binding)
        setSupportActionBar(findViewById(R.id.toolbar))
        toolText.text = "مشاهده ی جزئیات ملک"
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        mainLayout.visibility = View.GONE
        val propertyId = intent.getIntExtra("id", -1)
        val userStore = UserStore(this)
        adminCode =
                if (userStore.getUserType() == Const.USER_TYPE_ADMIN) {
                    (userStore.getUser() as Admin).id
                } else -2
        if (adminCode == -2 || propertyId == -1) {
            System.err.println("${userStore.userLogged } and ${userStore.getUserType()}")
            Tools.toast(this, "مشکلی در احراز هویت بوجود آمده است... لطفا دوباره وارد شوید.")
            userStore.logout()
            startActivity(Intent(this, SplashActivity::class.java))
            finishAffinity()
        } else {
            progressBar.visibility = View.VISIBLE
            DataBaseUtils().getProperty(this, propertyId)
        }
    }

    /**
     * Verification result
     */
    override fun onComplete(result: Boolean, responseCode: Int) {
        if (result && responseCode == Const.VERIFIED) {
            Tools.toast(this, "تایید ملک با موفقیت انجام شد.")
        } else {
            Tools.toast(this, "تایید ملک ناموفق بود.")
        }
    }

    /**
     * Get property Info and set textViews
     */
    override fun onComplete(result: Any?, e: Throwable?, responseCode: Int) {
        if (result != null && e == null && responseCode == Const.RESULT_HAS_DATA) {
            //Data is available
            progressBar.visibility = View.GONE
            mainLayout.visibility = View.VISIBLE
            val property = result as Property
            initView(property)
        } else {
            Tools.toast(this, "مشکلی در بدست آوردن اطلاعات آمده است. دوباره امتحان کنید.")
            Log.e("PropertyDetailActivity", e?.message)
            finish()
        }
    }

    private fun initView(p: Property) {
        pType.text = p.pType
        dType.text = p.dType
        size.text = p.size.toString()
        age.text = p.age.toString()
        city.text = DataBaseUtils().getCityByCode(p.cityCode)
        val lat = p.lat
        val lng = p.lng
        location.setOnClickListener {
            startActivity(Intent(this, ShowMapActivity::class.java)
                    .putExtra("lat", lat)
                    .putExtra("lng", lng))
        }
        price.text =
                if (p.dType == Const.dealTypes[Const.DEAL_RENT]) {
                    "رهن: " + p.price + " میلیون تومان" + "\n" +
                            "اجاره: " + p.ejare + " هزارتومان"
                } else {
                    p.price.toString() + " میلیون تومان"
                }
        ownerInfo.text = "تلفن: " + p.ownerPhone
        verified.text =
                if (p.adminCode == -1) {
                    verify.visibility = View.VISIBLE
                    verify.setOnClickListener {
                        AlertDialog.Builder(this)
                                .setTitle("تایید ملک")
                                .setMessage("آیا از تایید ملک اطمینان دارید؟\n" +
                                        "ملک برای مشتریان قابل مشاهده خواهد بود.")
                                .setPositiveButton("بله") {d,_ ->
                                    DataBaseUtils().verifyProperty(this, adminCode, p.id)
                                    d.dismiss()
                                }
                                .setNegativeButton("خیر", null)
                                .create()
                                .show()
                    }
                    "تاییده نشده"
                } else {
                    verify.visibility = View.GONE
                    p.adminCode.toString()
                }
        date.text = Tools.convertTimestampToDate(p.dateCreatedStamp)
    }
}
