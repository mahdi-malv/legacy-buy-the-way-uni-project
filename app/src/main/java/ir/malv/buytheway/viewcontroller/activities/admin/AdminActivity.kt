package ir.malv.buytheway.viewcontroller.activities.admin

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.squareup.picasso.Picasso
import ir.malv.buytheway.R
import ir.malv.buytheway.databinding.ActivityAdminBinding
import ir.malv.buytheway.databinding.Toolbar1Binding
import ir.malv.buytheway.model.Const
import ir.malv.buytheway.model.user.UserStore
import ir.malv.buytheway.viewcontroller.activities.LoginActivity
import ir.malv.buytheway.viewcontroller.activities.MyAppCompatActivity

class AdminActivity : MyAppCompatActivity() {

    lateinit var binding: ActivityAdminBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAdminBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        val toolbar1 = Toolbar1Binding.bind(view)
        val toolbar = toolbar1.toolbar
        setSupportActionBar(toolbar)
        toolbar1.toolText.text = "پنل مدیریت"

        init()
        binding.list.addOnItemTouchListener(ListClickListener(this, binding.list,
                object : ListClickListener.ClickListener {
            override fun onClick(view: View, position: Int) {
                val i = Intent(this@AdminActivity, DetailActivity::class.java)
                when(position) {
                    0 -> {
                        //Deals
                        i.putExtra(Const.ACTION_KEY, Const.ACTION_SEE_DEALS)
                    }
                    1 -> {
                        //Properties
                        i.putExtra(Const.ACTION_KEY, Const.ACTION_SEE_PROPERTIES)
                    }
                    2 -> {
                        //Customers
                        i.putExtra(Const.ACTION_KEY, Const.ACTION_SEE_CUSTOMERS)
                    }
                }
                startActivity(i)
            }
        }))
    }


    private var backPressed: Long = 0
    override fun onBackPressed() {
        if (backPressed + 2000 > System.currentTimeMillis()) {
            super.onBackPressed()
            finishAffinity()
        } else {
            Toast.makeText(applicationContext, "برای خروج دوباره بازگشت را بزنید.", Toast.LENGTH_SHORT).show()
            backPressed = System.currentTimeMillis()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menu?.add(0, 0, 0, "خروج از حساب")
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == 0) {
            UserStore(this).logout()
            startActivity(Intent(this, LoginActivity::class.java))
            finishAffinity()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    //region List
    private fun init() {
        binding.list.layoutManager = GridLayoutManager(this, 2)
        binding.list.setHasFixedSize(true)
        //region set
        val set = listOf<Data>(
                Data(
                        "http://riverraftinginindia.in/images/deal.png",
                        "معاملات"
                ),
                Data(
                        "http://ambitionsmba.com/wp-content/uploads/2018/03/register.png",
                        "ثبت شده ها"
                ),
                Data(
                        "http://files.softicons.com/download/business-icons/dragon-soft-icons-by-artua.com/png/512/User.png",
                        "مشتریان"
                )
        )
        //endregion
        binding.list.adapter = Adapter(set)
    }

    inner class Adapter(val set: List<Data>) : RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(
                    LayoutInflater.from(parent.context)
                            .inflate(R.layout.admin_item, parent, false)
            )
        }

        override fun getItemCount() = set.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.text.text = set[position].text
            Picasso.get().load(set[position].imgLink).into(holder.image)
        }

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image = itemView.findViewById<ImageView>(R.id.image)!!
        val text = itemView.findViewById<TextView>(R.id.text)!!
    }

    data class Data(var imgLink: String, var text: String)
    //endregion
}
