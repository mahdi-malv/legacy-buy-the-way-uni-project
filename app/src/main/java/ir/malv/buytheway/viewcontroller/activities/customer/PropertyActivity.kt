package ir.malv.buytheway.viewcontroller.activities.customer

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatDelegate
import android.view.View
import android.widget.Toast
import ir.malv.buytheway.R
import ir.malv.buytheway.model.*
import ir.malv.buytheway.model.database.entities.Customer
import ir.malv.buytheway.model.database.entities.Deal
import ir.malv.buytheway.model.database.entities.Property
import ir.malv.buytheway.model.user.UserStore
import ir.malv.buytheway.viewcontroller.activities.MyAppCompatActivity
import ir.malv.buytheway.viewcontroller.activities.both.ShowMapActivity
import kotlinx.android.synthetic.main.activity_property.*
import kotlinx.android.synthetic.main.toolbar_1.*

class PropertyActivity : MyAppCompatActivity(), Result, Response {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property)
        toolText.text = "مشاهده ی جزئیات ملک"
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        mainLayout.visibility = View.GONE
        val propertyId = intent.getIntExtra("id", -1)

        if (propertyId == -1) {
            Tools.toast(this, "شناسه ی ملک درست دریافت نشد.")
            finish()
        } else {
            progressBar.visibility = View.VISIBLE
            DataBaseUtils().getProperty(this, propertyId)
        }
    }

    override fun onComplete(result: Any?, e: Throwable?, responseCode: Int) {
        progressBar.visibility = View.GONE
        mainLayout.visibility = View.VISIBLE
        if (result != null && e == null && responseCode == Const.RESULT_HAS_DATA) {
            val property = result as Property
            initView(property)
        }
    }

    private fun initView(p: Property) {
        pType.text = p.pType
        dType.text = p.dType
        size.text = p.size.toString()
        age.text = p.age.toString()
        city.text = DataBaseUtils().getCityByCode(p.cityCode)
        val lat = p.lat
        val lng = p.lng
        location.setOnClickListener {
            startActivity(Intent(this, ShowMapActivity::class.java)
                    .putExtra("lat", lat)
                    .putExtra("lng", lng))
        }
        price.text =
                if (p.dType == Const.dealTypes[Const.DEAL_RENT]) {
                    "رهن: " + p.price + " میلیون تومان" + "\n" +
                            "اجاره: " + p.ejare + " هزارتومان"
                } else {
                    p.price.toString() + " میلیون تومان"
                }
        ownerInfo.text = "تلفن: " + p.ownerPhone
        deal.setOnClickListener {
            val store = UserStore(this)
            if (store.userLogged && store.getUserType() == Const.USER_TYPE_CUSTOMER) {
                progressBar.bringToFront()
                progressBar.visibility = View.VISIBLE
                val user = store.getUser() as Customer
                val deal = Deal(user.phone, p.id, System.currentTimeMillis())
                DataBaseUtils().addDeal(this, deal)
            }
        }
        date.text = Tools.convertTimestampToDate(p.dateCreatedStamp)
    }

    override fun onComplete(result: Boolean, responseCode: Int) {
        progressBar.visibility = View.GONE
        if (result && responseCode == 0) {
            Tools.alert(this,
                    "تبریک!",
                    "معامله با موفقیت انجام شد. بزودی با شما برای تکمیل قرارداد حضوری تماس گرفته میشود.")
                    .setPositiveButton("تایید") {d,_ ->
                        d.dismiss()
                        finish()
                    }
                    .create().show()
        } else {
            val message = when(responseCode) {
                0 -> "درج اطلاعات در دیتابیس ناموفق"
                1 -> "اطلاعات بدرستی درج نشده است."
                2 -> "این معامله قبلا توسط شما یا کسی دیگر انجام شده است."
                else -> "خداییش نداشتیم این کد خطا رو!"
            }
            Tools.toast(this, message)
        }
    }
}
