package ir.malv.buytheway.viewcontroller.activities.customer

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v7.app.AlertDialog
import android.support.v7.content.res.AppCompatResources
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import ir.malv.buytheway.R
import ir.malv.buytheway.model.Const
import ir.malv.buytheway.model.DataBaseUtils
import ir.malv.buytheway.model.Response
import ir.malv.buytheway.model.Tools
import ir.malv.buytheway.model.database.entities.Customer
import ir.malv.buytheway.model.database.entities.Property
import ir.malv.buytheway.model.user.UserStore
import ir.malv.buytheway.viewcontroller.activities.MyAppCompatActivity
import ir.malv.buytheway.viewcontroller.activities.SplashActivity
import kotlinx.android.synthetic.main.activity_add_p.*
import kotlinx.android.synthetic.main.toolbar_1.*

class AddPActivity : MyAppCompatActivity(), Response, AdapterView.OnItemSelectedListener {

    private lateinit var dealAdapter: ArrayAdapter<String>
    private lateinit var propertyAdapter: ArrayAdapter<String>
    private var isLocationSelected = false
    private var lat = (0).toDouble()
    private var lng = (0).toDouble()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_p)
        val t = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(t)
        toolText.text = "افزودن ملک به سامانه"
        init()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        isLocationSelected =
                if (intent == null) {
                    val colorList = AppCompatResources.getColorStateList(this, R.color.colorAccent)
                    ViewCompat.setBackgroundTintList(selectLocation, colorList)
                    selectLocation.text = "انتخاب از نقشه"
                    false
                } else {
                    val colorList = AppCompatResources.getColorStateList(this, R.color.material_blue)
                    ViewCompat.setBackgroundTintList(selectLocation, colorList)
                    selectLocation.text = "انتخاب مجدد"
                    lat = intent.getDoubleExtra("lat", (-1).toDouble())
                    lng = intent.getDoubleExtra("lng", (-1).toDouble())
                    true
                }
    }

    /**
     * Why is #onItemSelectedListener of dealType here?
     * I want it to be checked when activity starts (Not create)
     * onCreate only invokes once but onStart does more.
     */
    override fun onStart() {
        super.onStart()
        progressBar.visibility = View.GONE
        dealTypeSpinner.onItemSelectedListener = this
    }

    /**
     * This function will set adapters and handles clicks
     */
    private fun init() {
        cityEdit.setAdapter(ArrayAdapter<String>(this, R.layout.spinner_item, Const.cities))
        dealAdapter = ArrayAdapter(this, R.layout.spinner_item, Const.dealTypes)
        dealTypeSpinner.adapter = dealAdapter
        propertyAdapter = ArrayAdapter(this, R.layout.spinner_item, Const.propertyTypes)
        propertyTypeSpinner.adapter = propertyAdapter
        selectLocation.setOnClickListener {
            startActivity(Intent(this, MapsActivity::class.java))
        }

        //region Confirm button
        confirmFab.setOnClickListener {
            //Validate city
            val isCityOk = !TextUtils.isEmpty(cityEdit.text.toString()) &&
                    DataBaseUtils().getCodeByCity(cityEdit.text.toString()) != -1
            //Validate size
            val isSizeOk = !TextUtils.isEmpty(sizeEdit.text.toString()) &&
                    Tools.getNumber(sizeEdit) in 20..1000000
            //Validate age
            val isAgeOk = !TextUtils.isEmpty(ageEdit.text.toString()) &&
                    Tools.getNumber(ageEdit) in 0..60
            //Validate price by Deal type
            val isPriceOk =
                    if (dealTypeSpinner.selectedItemPosition == 0) {
                        //Sell
                        !TextUtils.isEmpty(priceEdit.text) && Tools.getNumber(priceEdit) in 0..100000
                    } else {
                        //Rahn & Ejare
                        (!TextUtils.isEmpty(rahnEdit.text) && Tools.getNumber(rahnEdit) in 0..1000) &&
                                (!TextUtils.isEmpty(ejareEdit.text) && Tools.getNumber(ejareEdit) in 0..10000)
                    }
            //Validate comment
            val isCommentOk = Tools.getString(commentEdit).length < 500

            //if (AllAreOk)
            System.err.println("$isCityOk $isAgeOk $isPriceOk $isSizeOk $isCommentOk $isLocationSelected")
            if (isCityOk && isAgeOk && isPriceOk && isSizeOk && isCommentOk && isLocationSelected) {
                AlertDialog.Builder(this)
                        .setTitle("ارسال اطلاعات")
                        .setMessage("آیا از ارسال اطلاعات برای تایید توسط مدیریت اطمینان دارید؟")
                        .setPositiveButton("بله") { _, _ ->
                            if (UserStore(this).getUserType() != Const.USER_TYPE_CUSTOMER) {
                                Toast.makeText(this, "اطلاعات شما درست ذخیره نشده...", Toast.LENGTH_SHORT).show()
                                UserStore(this).logout()
                                finishAffinity()
                                startActivity(Intent(this, SplashActivity::class.java))
                            } else {
                                //region PropertyObject
                                val property = Property(
                                        0,
                                        (UserStore(this).getUser() as Customer).phone,
                                        sizeEdit.text.toString().toInt(),
                                        ageEdit.text.toString().toInt(),
                                        Const.dealTypes[dealTypeSpinner.selectedItemPosition],
                                        Const.propertyTypes[propertyTypeSpinner.selectedItemPosition],
                                        DataBaseUtils().getCodeByCity(cityEdit.text.toString()),
                                        lat,
                                        lng,
                                        commentEdit.text.toString(),
                                        -1,
                                        System.currentTimeMillis(),
                                        if (dealTypeSpinner.selectedItemPosition == Const.DEAL_SELL)
                                            priceEdit.text.toString().toInt()
                                        else rahnEdit.text.toString().toInt(),
                                        if (dealTypeSpinner.selectedItemPosition == Const.DEAL_RENT)
                                            ejareEdit.text.toString().toInt()
                                        else -1
                                )
                                //endregion
                                setUpInfoAndMakeObject(property)
                            }
                        }
                        .setNegativeButton("خیر", null)
                        .create().show()
            } else {
                Toast.makeText(this, "فیلد ها معتبر نیستند...", Toast.LENGTH_SHORT).show()
            }
        }
        //endregion
    }

    private fun setUpInfoAndMakeObject(p: Property) {
        progressBar.visibility = View.VISIBLE
        DataBaseUtils().addProperty(this, p)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        //Only for deal type
        if (position == 0) {
            //Sell - Price visible - rent hidden
            priceLayout.visibility = View.VISIBLE
            rentLayout.visibility = View.GONE
        } else if (position == 1) {
            //Sell - Price hidden - rent visible
            priceLayout.visibility = View.GONE
            rentLayout.visibility = View.VISIBLE
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        return
    }

    override fun onComplete(result: Boolean, responseCode: Int) {
        progressBar.visibility = View.GONE
        if (result) {
            AlertDialog.Builder(this)
                    .setTitle("تبریک!")
                    .setMessage("ملک شما برای تایید به سمت مدیریت فرستاده شد.\n" +
                            "شناسه ی ملک: $responseCode")
                    .setPositiveButton("باشه") { _, _ ->
                        finish()
                        startActivity(Intent(this, CustomerActivity::class.java))
                    }
                    .create()
                    .show()
        } else {
            Toast.makeText(this, "مشکلی در افزودن ملک در سامانه بوجود آمده. لطقا مجددا امتحان نمایید.",
                    Toast.LENGTH_SHORT).show()
        }
    }
}
