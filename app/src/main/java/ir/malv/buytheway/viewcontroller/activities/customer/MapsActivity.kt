package ir.malv.buytheway.viewcontroller.activities.customer

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AlertDialog
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import ir.malv.buytheway.R
import ir.malv.buytheway.model.Const
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class MapsActivity : FragmentActivity(), OnMapReadyCallback, GoogleMap.OnCameraMoveStartedListener, GoogleMap.OnCameraIdleListener {

    private var mMap: GoogleMap? = null
    lateinit var cursor: FloatingActionButton

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        init()
    }

    private fun init() {
        cursor = findViewById(R.id.cursor)
        cursor.size = FloatingActionButton.SIZE_MINI
        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            val latLng = mMap!!.cameraPosition.target

            AlertDialog.Builder(this@MapsActivity)
                    .setTitle("محل ملک")
                    .setMessage("آیا مکان انتخاب شده درست است؟")
                    .setPositiveButton("بله") { _, _ ->
                        startActivity(Intent(this@MapsActivity, AddPActivity::class.java)
                                .putExtra("lat", latLng.latitude)
                                .putExtra("lng", latLng.longitude))
                        finish()
                    }.setNegativeButton("خیر", null)
                    .create().show()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val latLng = LatLng(intent.getDoubleExtra("lat", Const.IRAN_LAT),
                intent.getDoubleExtra("lng", Const.IRAN_LNG))
        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 6.0f))
        mMap!!.setOnCameraMoveStartedListener(this)
        mMap!!.setOnCameraIdleListener(this)

    }

    override fun onCameraMoveStarted(i: Int) {
        cursor.size = FloatingActionButton.SIZE_MINI
    }

    override fun onCameraIdle() {
        cursor.size = FloatingActionButton.SIZE_NORMAL
    }

    override fun onBackPressed() {
        startActivity(Intent(this@MapsActivity, AddPActivity::class.java))
        finish()
    }
}

