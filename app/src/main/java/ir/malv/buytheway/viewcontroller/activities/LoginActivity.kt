package ir.malv.buytheway.viewcontroller.activities

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import ir.malv.buytheway.R
import ir.malv.buytheway.model.Const
import ir.malv.buytheway.model.DataBaseUtils
import ir.malv.buytheway.model.Response
import ir.malv.buytheway.model.Tools
import ir.malv.buytheway.model.database.entities.Admin
import ir.malv.buytheway.model.user.UserStore
import ir.malv.buytheway.viewcontroller.activities.admin.AdminActivity
import ir.malv.buytheway.viewcontroller.activities.customer.CustomerActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : MyAppCompatActivity(), View.OnClickListener, Response {

    companion object {
        var isManager = false
    }
    private lateinit var phoneOrId: String
    private lateinit var pass: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
        isManager = false
        phoneEdit.hint = "Phone: 09..."
    }

    private fun init() {
        login.setOnClickListener(this)
        noAccount.setOnClickListener(this)
        manager.setOnClickListener(this)
    }

    //region validation of views
    private fun validateFields() {
        val areNotEmpty = !TextUtils.isEmpty(phoneEdit.text) && !TextUtils.isEmpty(passEdit.text)

        val isPhoneOrIdOk =
                if (!isManager) Tools.validatePhoneNumber(phoneEdit.text.toString())
                else phoneEdit.text.toString().toInt() >= 1

        val isPassOk = Tools.validatePassword(passEdit.text.toString())

        if (areNotEmpty && isPhoneOrIdOk && isPassOk) {
            phoneOrId = phoneEdit.text.toString()
            pass = passEdit.text.toString()
            DataBaseUtils().login(this, phoneOrId, pass)
            progress.visibility = View.VISIBLE
        } else {
            Toast
                    .makeText(this, "مقادیر نامعتبر", Toast.LENGTH_SHORT)
                    .show()
        }
    }
    //endregion

    override fun onBackPressed() {
        if (isManager) {
            isManager = false
            manager.visibility = View.VISIBLE
            noAccount.visibility = View.VISIBLE
            phoneEdit.setText("")
            passEdit.setText("")
            phoneEdit.hint = "Phone: 09..."
        } else {
            super.onBackPressed()
        }
    }

    override fun onClick(v: View?) {
        try {
            when (v!!.id) {
                R.id.login -> {
                    validateFields()
                    Tools.hideSoftKeyboard(this)
                }
                R.id.noAccount -> {
                    startActivity(Intent(this, RegisterActivity::class.java))
                }
                R.id.manager -> {
                    isManager = true
                    phoneEdit.hint = "ID"
                    phoneEdit.setText("")
                    passEdit.setText("")
                    manager.visibility = View.GONE
                    noAccount.visibility = View.GONE
                }
            }
        } catch (e: Exception) {
            Log.e("EXCEPTION", e.message)
        }
    }

    override fun onComplete(result: Boolean, responseCode: Int) {
        progress.visibility = View.GONE
        if (result) {
            if (isManager) {
                startActivity(Intent(this, AdminActivity::class.java))
            }
            else
                startActivity(Intent(this, CustomerActivity::class.java))
            finishAffinity()
        } else {
            if (responseCode == Const.CUSTOMER_LOGIN_FAILED) {
                Toast.makeText(this, "تلفن یا رمز اشتباه", Toast.LENGTH_SHORT).show()
            } else if (responseCode == Const.MANAGER_LOGIN_FAILED) {
                Toast.makeText(this, "شناسه یا رمز اشتباه", Toast.LENGTH_SHORT).show()
            } else if (responseCode  == Const.MANAGER_LOGIN || responseCode == Const.CUSTOMER_LOGIN) {
                Toast.makeText(this, "رمز اشتباه", Toast.LENGTH_SHORT).show()
            }

        }
    }
}

