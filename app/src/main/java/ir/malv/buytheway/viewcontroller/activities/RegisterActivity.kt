package ir.malv.buytheway.viewcontroller.activities

import android.content.Intent
import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.InputType
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.Toast
import ir.malv.buytheway.R
import ir.malv.buytheway.model.Const
import ir.malv.buytheway.model.DataBaseUtils
import ir.malv.buytheway.model.Response
import ir.malv.buytheway.model.Tools
import ir.malv.buytheway.viewcontroller.activities.admin.AdminActivity
import ir.malv.buytheway.viewcontroller.activities.customer.CustomerActivity
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : MyAppCompatActivity(), Response {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        LoginActivity.isManager = false
        init()
    }

    override fun onStart() {
        super.onStart()
        initChangeableTextFields()
    }

    private fun initChangeableTextFields() {
        val watcher = object : TextWatcher() {
            override fun afterTextChanged(s: Editable?) {
                val isNameOk =
                        if (LoginActivity.isManager) Tools.getNumber(nameEdit) > 0
                        else Tools.validateName(s.toString())
                if (!isNameOk) {
                    nameLayout.isErrorEnabled = true
                    nameLayout.error = if (LoginActivity.isManager) "شناسه نامعتبر" else "نام نامعتبر"
                } else
                    nameLayout.isErrorEnabled = false
            }
        }
        nameEdit.inputType = if (LoginActivity.isManager)
            InputType.TYPE_CLASS_NUMBER else InputType.TYPE_CLASS_TEXT
        phoneEdit.visibility = if (LoginActivity.isManager) View.GONE else View.VISIBLE
        nameLayout.hint = if (LoginActivity.isManager) "ID" else "Name"
        nameEdit.addTextChangedListener(watcher)
    }

    override fun onBackPressed() {
        if (LoginActivity.isManager) {
            LoginActivity.isManager = false
            iAmAdmin.visibility = View.VISIBLE
            initChangeableTextFields()
        } else {
            super.onBackPressed()
        }
    }

    /**
     * Holding TextWatchers and Clicks
     */
    private fun init() {
        //region TextWatchers
        phoneEdit.addTextChangedListener(object : TextWatcher() {
            override fun afterTextChanged(s: Editable?) {
                val isPhoneOk = Tools.validatePhoneNumber(s.toString())
                if (!isPhoneOk) {
                    phoneLayout.isErrorEnabled = true
                    phoneLayout.error = "شماره تلفن نامعتبر"
                } else
                    phoneLayout.isErrorEnabled = false
            }
        })

        passEdit.addTextChangedListener(object : TextWatcher() {
            override fun afterTextChanged(s: Editable?) {
                val isPassOk = Tools.validatePassword(s.toString())
                if (!isPassOk) {
                    passLayout.isErrorEnabled = true
                    passLayout.error = "رمز نامعتبر"
                } else
                    passLayout.isErrorEnabled = false
            }
        })
        //endregion
        //region Clicks
        iAmAdmin.setOnClickListener {
            LoginActivity.isManager = true
            it.visibility = View.GONE
            initChangeableTextFields()
        }

        signUp.setOnClickListener {
            signUp.text = ""
            progressBar.bringToFront()
            progressBar.visibility = View.VISIBLE
            val nameOk =
                    if (LoginActivity.isManager) {
                        !TextUtils.isEmpty(nameEdit.text) && Tools.getNumber(nameEdit) > 0
                    } else {
                        !TextUtils.isEmpty(nameEdit.text) && Tools.validateName(nameEdit.text.toString())
                    }
            val passOk = !TextUtils.isEmpty(passEdit.text) && Tools.validatePassword(passEdit.text.toString())
            val phoneOk = LoginActivity.isManager || (!TextUtils.isEmpty(phoneEdit.text) &&
                    Tools.validatePhoneNumber(phoneEdit.text.toString()))

            if (nameOk && passOk && phoneOk) {
                if (LoginActivity.isManager)
                    try {

                        val e = EditText(this)
                        e.inputType = InputType.TYPE_CLASS_NUMBER
                        e.tag = "token"
                        AlertDialog.Builder(this)
                                .setTitle("احراز هویت")
                                .setMessage(getString(R.string.please_enter_token))
                                .setView(e)
                                .setPositiveButton("تایید") { d, _ ->
                                    if (e.text.toString() == "1234567890") {
                                        d.dismiss()
                                        DataBaseUtils().registerAdmin(this, Tools.getNumber(nameEdit), passEdit.text.toString())
                                    } else {
                                        Toast.makeText(this, "کد اشتباه", Toast.LENGTH_SHORT).show()
                                        signUp.text = "ثبت نام"
                                        progressBar.visibility = View.GONE
                                    }
                                }
                                .setNegativeButton("انصراف") {d,_ ->
                                    signUp.text = "ثبت نام"
                                    progressBar.visibility = View.GONE
                                    d.dismiss()
                                }
                                .create()
                                .show()
                    } catch (e: SQLiteConstraintException) {
                        Toast.makeText(this, "شناسه قبلا گرفته شده", Toast.LENGTH_SHORT).show()
                    }
                else
                    try {
                        DataBaseUtils().registerCustomer(this, nameEdit.text.toString(),
                                passEdit.text.toString(), phoneEdit.text.toString())
                    } catch (e: SQLiteConstraintException) {
                        Toast.makeText(this, "شماره قبلا گرفته شده", Toast.LENGTH_SHORT).show()
                    }
            }
        }
        //endregion
    }

    override fun onComplete(result: Boolean, responseCode: Int) {
        signUp.text = "ثبت نام"
        progressBar.visibility = View.GONE
        if (result) {
            if (responseCode == Const.MANAGER_SIGNUP)
                startActivity(Intent(this, AdminActivity::class.java))
            else if (responseCode == Const.CUSTOMER_SIGNUP)
                startActivity(Intent(this, CustomerActivity::class.java))
            finishAffinity()
        } else {
            Toast.makeText(this, "افزودن حساب به پایگاه داده ناموفق بود. لطفا مجددا امتحان کنید.",
                    Toast.LENGTH_LONG).show()
        }
    }
}

abstract class TextWatcher : android.text.TextWatcher {

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }
}
