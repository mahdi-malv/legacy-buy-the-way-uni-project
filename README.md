# Buy The Way
Simple **Room** ORM **SQLite** project in android (With Kotlin lang)

### This project can be used for university course projects or bachelor project (If you improve it).

This project uses Android <a href="https://developer.android.com/topic/libraries/architecture/room">Room persistence library</a> from Google and <a href="https://github.com/ReactiveX/RxJava">RxJava</a>.<br>
The simulation is a System for **property** deals (An online consultant).
